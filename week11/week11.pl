use strict;
use warning;

my $file = $ARGV[0];

open IN, "<", $file or die "can not open $file : $!\n";

my @lines = <IN>;

close IN;

foreach my $line (@lines){

	chomp $line;
	
	my $host = ""; my $time = ""; my $status = 0;
	
	if($line =~ /^(.*)\s(.*\s){2}\[(.*)\]\s".*"\s(\d{3})\s(\d+|-)/){
	
		$host = $1;
		$time = $2;
		$status = $3;
		
		print "$host /t$time\t$status\n";
	}

}