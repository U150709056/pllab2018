use strict;

my @arr = ("Ali","Mehmet","Wahab");

my $size = @arr;

print $size, "\n";

my ($a , $b , $c , $d) = @arr;

print  "$a , $b , $c , $d\n";

my @arr2 = qw/This is just a sentence./;

print "@arr2\n";

print "$arr2[2]\n";